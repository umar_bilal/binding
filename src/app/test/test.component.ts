import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  
  public name     = 'Umar Bilal';
  public siteUrl  = window.location.href;
  public myId     = "testId";
  public success = "text-success";
  public italic  = "text-special";
  public hasError = false;
  public isSpecial = true;
  public highLightColor = "grey";

  public msgClasses = {
    "text-success": !this.hasError,
    "text-danger": this.hasError,
    "text-special": this.isSpecial
  }

  public titleStyles = {
    color: "pink",
    fontStyle: "italic"
  }

  constructor() { }

  ngOnInit() {
  }

  greetUser(){
    return "Hello " + this.name;
  }

}
