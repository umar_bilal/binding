import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-eventlis',
  templateUrl: './eventlis.component.html',
  styleUrls: ['./eventlis.component.css']
})
export class EventlisComponent implements OnInit {

  public greeting = '';
  constructor() { }

  ngOnInit() {
  }

  onClick(event){
    console.log(event);
    this.greeting = 'Welcome to the application.';
  }

}
